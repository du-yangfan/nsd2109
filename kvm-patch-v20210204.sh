#/bin/bash
grep  -qw  'server1'  /etc/hosts  ||  echo  '172.25.0.254  server1.net0.example.com  server1.lab0.example.com  server1' >> /etc/hosts

sed -i 's/^enabled=1/enabled=0/g' /etc/dnf/plugins/subscription-manager.conf

yum  -y  install  httpd 2> /dev/null
tar  -xPf  kvm-patch-v2021*.tgz
ln  -s  /var/ftp/rhel  /var/www/html/rhel8  &> /dev/null
ln  -s  /var/ftp/rhel  /var/www/html/rhel8.2  &> /dev/null

echo '<html>
<title>Exam</title>
<body>
<h1>exam</h1>
  <p>
  <a href="/exam/EX200.pdf">EX200</a>
  </p>
  <p>
  <a href="/exam/EX294.pdf">EX294</a>
  </p>
</body>
</html>' > /var/www/html/index.html
systemctl enable  httpd  ; systemctl restart  httpd  
systemctl disable  firewalld ; systemctl  stop  firewalld

yum  -y  install  chrony 2> /dev/null
grep -q '172.25' /etc/chrony.conf || echo 'allow 172.25.254.0/24
allow 172.25.0.0/24' >> /etc/chrony.conf
systemctl  enable  chronyd ; systemctl  restart  chronyd

groupadd -g 1040 ldapuser0 &> /dev/null
mkdir  -p  /rhome
useradd -u 1040 -g 1040 -d /rhome/ldapuser0 ldapuser0  &> /dev/null
echo password | passwd --stdin ldapuser0

echo  '/rhome   *(rw)'  >  /etc/exports
systemctl  enable  nfs-server ; systemctl  restart  nfs-server

virsh list --all | grep -qw utilitya || rht-vmctl  reset  utility
sleep  3  &&  virsh  destroy  utility &> /dev/null

echo '
######################
## kvm-patched OK.  ##
######################
'
